# Simple Blockchain

### How to start

Recommended to use Node 8 or greater and with nvm.
```bash
nvm use && npm i # If you dont have nvm installed, you can just `npm i`

npm run demo # Start DEMO creates Genesis + 3 blocks and validates the blockchain
```

### To test
Demonstrates that modifying a block ends up invalidating the blockchain
```bash
npm test
# Start DEMO creates Genesis + 3 blocks and validates the blockchain
# Then modify a block (2) and validates the blockchain again => Invalid block(2)
```

### Structure

- `demo.js` and `test.js` -  Demo and test file (calling APIs from `blockchain.js`)
- `blockchain.js` - Block and Blockchain classes (calls `db.js` methods)
- `db.js` - LevelDB inetraction layer

### Reference

- [LevelDB](https://github.com/Level/level)