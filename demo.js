const { Block, Blockchain } = require('./blockchain');

async function main() {
  const blockchain = await new Blockchain();

  setTimeout(async () => {
    await blockchain.addBlock(new Block('TEST BLOCK 1'));
  }, 100);

  setTimeout(async () => {
    await blockchain.addBlock(new Block('TEST BLOCK 2'));
  }, 200);

  setTimeout(async () => {
    await blockchain.addBlock(new Block('TEST BLOCK 3'));
  }, 300);

  setTimeout(async () => {
    await blockchain.getBlock(3);
  }, 400);

  setTimeout(async () => {
    await blockchain.getBlockHeight();
  }, 500);

  setTimeout(async () => {
    await blockchain.validateChain();
  }, 600);

  setTimeout(async () => {
    await blockchain.getAllBlocks();
  }, 700);
}

main();
